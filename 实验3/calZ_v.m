function [ z_v ] = calZ_v( t,splitTime)
%返回z轴方向上的速度z_v
%   t为当前时间，splitTime为分割时间大小,totalTime为仿真总时间，为splitTime的整数倍

current_unit=ceil(t/splitTime);      %向上取整
if mod(current_unit,2)==0        %取余2
    z_v=1;
else
    z_v=-1;
end

end

