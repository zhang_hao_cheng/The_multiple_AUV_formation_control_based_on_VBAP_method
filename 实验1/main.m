clc;clear all

%% 测试V_h_c函数
% alpha_h=10;
% h0=2;
% h1=100;
% x=[1,2;2,1];
% b=[1,1;2,2];
% [V_h_c,V_h_c_grad]=V_h_c(10,2,100,x,b);

%% 测试V_h_c函数
% alpha_I=10;
% d0=2*sqrt(3);
% d1=100;
% %xi=[1,2];
% %xj=[2,3];
% [V_I_ij,V_I_ij_grad]=V_i(alpha_I,d0,d1);

%% 实验1---2017年11月30日
% x1_0=[0,sqrt(3)];                          %第一个AUV的位置坐标
% x2_0=[0,-sqrt(3)];                         %第二个AUV的位置坐标
% b1_0=[1,0];                                %第一个虚拟领导者的位置坐标
% b2_0=[-1,0];                               %第二个虚拟领导者的位置坐标
% v0=1;                                      %期望速度
% fai_u=1.1;                                 %误差上界
% zeta=0.001;                                %很小的正系数
% alpha_h=10;
% h0=2;                                      %AUV和虚拟领导者间的距离
% h1=100;                                    %h0的上界
% alpha_I=10;
% d0=2*sqrt(3);                              %两AUV间的距离
% d1=100;                                    %d0的上界
% r=[0,0];                                   %队列中心坐标
% 
% [V_I_ij,V_I_ij_grad]=V_i(alpha_I,d0,d1);
% x_0=[x1_0,x2_0];
% b_0=[b1_0,b2_0];
% [V_h_c,V_h_c_grad]=V_h_c(10,2,100,x_0,b_0);
% y=[x1_0;0,0;x2_0;0,0;0,0];
[t,y]=ode45('Function',[0 30],[0,0,0,sqrt(3),0,0,0,-sqrt(3),0,0.9,0]);
%point=[y(:,2) y(:,4)];
% hold on
% y1=pi/2*ones(size(y(:,2)));
% plot(t,y1)

