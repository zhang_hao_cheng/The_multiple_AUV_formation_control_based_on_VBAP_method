function [ V_I_ij ] = V_i(alpha_I,d0,d1,xij)
%V_I 返回AUV间的人工势能
%   xij代表第i个AUV到第j个AUV间的距离

if xij>0 && (xij<=d1)
    V_I_ij=alpha_I*(1/3*xij^3-d0^3*log(xij)-1/3*d0^3+d0^3*log(d0));
else
    V_I_ij=0;
end

